from base64 import b64encode
import threading
import time

import cv2
from django.shortcuts import render
from django.http import StreamingHttpResponse, JsonResponse

class VideoCamera(object):
    def __init__(self):
        self.video = cv2.VideoCapture(0)
        (self.grabbed, self.frame) = self.video.read()
        threading.Thread(target=self.update, args=()).start()

    def __del__(self):
        self.video.release()

    def get_frame(self):
        image = self.frame
        ret, jpeg = cv2.imencode('.jpg', image)
        return jpeg.tobytes()

    def update(self):
        while True:
            (self.grabbed, self.frame) = self.video.read()

cam = VideoCamera()

def index(request):
    image_data = "data:image/jpg;base64," + b64encode(cam.get_frame()).decode('utf-8')

    context = {
        'img': image_data,
    }

    return render(request, 'index.html', context)

def take_picture(request):
    t = int(time.time())

    cv2.imwrite(f'{t}.jpg', cam.frame)
    return JsonResponse({'data': f'{t}.jpg'})

def get_picture(request):
    image_data = "data:image/jpg;base64," + b64encode(cam.get_frame()).decode('utf-8')
    return JsonResponse({'data': image_data})

class VideoCamera(object):
    def __init__(self):
        self.video = cv2.VideoCapture(0)
        (self.grabbed, self.frame) = self.video.read()
        threading.Thread(target=self.update, args=()).start()

    def __del__(self):
        self.video.release()

    def get_frame(self):
        image = self.frame
        ret, jpeg = cv2.imencode('.jpg', image)
        return jpeg.tobytes()

    def update(self):
        while True:
            (self.grabbed, self.frame) = self.video.read()


def gen(camera):
    while True:
        frame = cam.get_frame()
        yield(b'--frame\r\n'
        b'Content-Type: image/jpeg\r\n\r\n' + frame + b'\r\n\r\n')


# @gzip.gzip_page
def livefe(request):
    try:
        return StreamingHttpResponse(gen(cam), content_type="multipart/x-mixed-replace;boundary=frame")
    except:
        pass
