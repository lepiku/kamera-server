from django.urls import path

from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('livefeed', views.livefe),
    path('take_picture', views.take_picture, name='take'),
    path('get_picture', views.get_picture, name='get'),
]
