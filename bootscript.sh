#!/bin/bash

while [ ! -e /dev/video0  ]
do
  sleep 2 # or less like 0.2
done

chmod 666 /dev/video0
source /home/user/compile-driver/webcam/bin/activate

cd /home/user/okteam/kamera
python manage.py runserver 0.0.0.0:8000
